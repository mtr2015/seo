<?php
$arg1 = 'first';
$arg2 = 'two';
$return = call_user_func(function(){
    $arg = func_get_arg(0); //func_get_arg函数作用：获取函数的第几个参数，必须要有参数，参数必须为函数参数的偏移量，0代表第一个参数
    $args = func_get_args();//func_get_args的作用：获取函数所有的参数
    if(func_num_args() == 1){//func_num_args函数的作用：获取函数参数的个数，注意，假如函数没有传参，该函数返回0
        return $args[0];
    }else{
        //用|把函数的参数组织成字符串
        return implode('|',$args);
    }
},$arg1,$arg2);
var_dump($return);
