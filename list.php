<?php
$my_array = array("Dog","Cat","Horse");

list($a, $b, $c) = $my_array;
echo "I have several animals, a $a, a $b and a $c.";


echo '<br/>';
$k = 7;
echo '调用函数前，'.$k;
echo '<br/>';

function changeVar(&$var){
    $var = 5;
    echo '函数内部'.$var;
    echo '<br/>';
}

changeVar($k);
echo '调用函数后，'.$k;
echo '<br/>';