<?php

if(!isset( $_SERVER['PATH_INFO'])){
    $pathinfo = 'default';
}else{
    $pathinfo =  explode('/', $_SERVER['PATH_INFO']);
}

if(is_array($pathinfo) && !empty($pathinfo)){
    $page = $pathinfo[1];
}else{
    $page = 'a';
}

require "$page.php";

?>