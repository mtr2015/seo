<?php
//先引用后增加
function _call($call){
    //通过传参获取call_user_func传过来的参数
    echo $call++,'<br/>';
    echo $call++,"<br/>";
}
//上面回调函数没有返回值，所以，这里就没有返回值，_call为上面的函数的名称
$re = call_user_func('_call',1);
//实验结果为 null，符合上面的结论
var_dump($re);
